package interfaces;

import entidades.Empresa;

public interface IPunto {
	public String getNombre();
	public Empresa getEmpresa();
	public String getCoordenadas();
}
