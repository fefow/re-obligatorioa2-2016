package interfaces;

import entidades.NodoTramo;
import entidades.Retorno;

public interface ISistema {

	public Retorno inicilizarSistema(int cantPuntos); // cumplida
	public Retorno destruirSistema();
	public Retorno registrarEmpresa(String nombre, String direccion, String pais, String email_contacto, String color);
	public Retorno registrarCiudad(String nombre, Double coordX, Double coordY);
	public Retorno registrarDC(String nombre, Double coordX, Double coordY, String Empresa, int capacidadCPUenHoras, int costoCPUporHora);
	public Retorno registrarTramo(Double coordXi, Double coordYi, Double coordXf, Double coordYf, int peso);
	public Retorno eliminarTramo(NodoTramo t);
	public Retorno eliminarPunto(Double coordX, Double coordY);
	public Retorno mapaEstado();
	public Retorno procesarInformacion(Double coordX, Double coordY, int esfuerzoCPUrequeriendoEnHora);
	public Retorno listadoRedMinima();
	public Retorno listadoEmpresas();

}
