package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import entidades.NodoHash;
import entidades.NodoTramo;
import entidades.Punto;
import entidades.Retorno;
import entidades.Retorno.Resultado;

public class tramos extends JFrame {

    private static final long serialVersionUID = 1L;
    private app_config app;
    private JPanel contentPane;
    private JComboBox txtPuntoX;
    private JComboBox txtPuntoY;
    private JTextField txtPeso;
    private JLabel lblPuntoX;
    private JLabel lblPuntoY;
    private JLabel lblPeso;
    private JButton btnAgregar;
    private JLabel lblMensaje; // para agregar
    private JLabel lblMensaje2; // para borrar
    private DefaultListModel<NodoTramo> nuevoModelo;
    private JList<NodoTramo> listTramos;

    public tramos(app_config appM)
    {
        app = appM;
        nuevoModelo = actualizarListTramos();
        setVisible(true);
        setDefaultCloseOperation(2);
        setBounds(100, 100, 558, 341);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lbltitulo = new JLabel("Agregar Tramos");
        lbltitulo.setBounds(20, 11, 300, 29);
        lbltitulo.setFont(new Font("Arial", Font.BOLD, 20));
        lbltitulo.setForeground(Color.black);
        contentPane.add(lbltitulo);
        
        txtPeso = new JTextField();
        txtPeso.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyTyped(KeyEvent e) {

	        	char c = e.getKeyChar();
	        	
	        	if(Character.isLetter(c)) {
	        		getToolkit().beep();
	        		lblMensaje.setText("Solo se permiten numeros");
	        		e.consume();
	        	}else{
		        	btnAgregar.setEnabled(
	            			txtPeso.getText().length() != -1
	            	);
		        	lblMensaje.setText("");
	        	}
        		
        		
        	}
        });
        
        txtPeso.setBounds(96, 61, 146, 20);
        txtPeso.setToolTipText("Ingrese peso del tramo");
        contentPane.add(txtPeso);
        txtPeso.setColumns(10);
        
        lblPeso = new JLabel("Peso:");
        lblPeso.setBounds(35, 64, 46, 14);
        contentPane.add(lblPeso);
        btnAgregar = new JButton("Agregar");
        btnAgregar.setEnabled(false);
        btnAgregar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if(txtPeso.getText().length() != 0){
        			crearTramo(Integer.parseInt(txtPeso.getText()),txtPuntoX.getSelectedItem().toString(),txtPuntoY.getSelectedItem().toString());
        		}else{
        			btnAgregar.setEnabled(false);
        		}
        	}
        });
        btnAgregar.setBounds(94, 155, 89, 23);
        
        contentPane.add(btnAgregar);
        
        lblMensaje = new JLabel("");
        lblMensaje.setBounds(35, 189, 207, 28);
        lblMensaje.setFont(new Font("Calibri", 0, 11));
        lblMensaje.setForeground(Color.DARK_GRAY);
        contentPane.add(lblMensaje);
        
        lblPuntoX = new JLabel("Punto:");
        lblPuntoX.setBounds(35, 95, 46, 14);
        contentPane.add(lblPuntoX);
        
        txtPuntoX = new JComboBox();
        txtPuntoX.setBounds(96, 92, 146, 20);
        contentPane.add(txtPuntoX);
        cargarPuntos(txtPuntoX);
        
        lblPuntoY = new JLabel("Punto:");
        lblPuntoY.setBounds(36, 126, 45, 14);
        contentPane.add(lblPuntoY);
        
        txtPuntoY = new JComboBox();
        txtPuntoY.setBounds(96, 123, 146, 20);
        contentPane.add(txtPuntoY);
        cargarPuntos(txtPuntoY);
        
        JLabel lblEliminarTramo = new JLabel("Eliminar Tramo");
        lblEliminarTramo.setBounds(336, 21, 136, 14);
        contentPane.add(lblEliminarTramo);

        
        JButton btnEliminarTramo = new JButton("Eliminar");
        btnEliminarTramo.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(listTramos.getSelectedValue() != null){
        			
        			NodoTramo tramo = (NodoTramo) listTramos.getSelectedValue();
        			int i = listTramos.getSelectedIndex();
        			
        			if(borrarTramo(tramo) == true){
        				
        				nuevoModelo.remove(i);
        			}
        			
        		}
        	}
        });
        btnEliminarTramo.setBounds(336, 251, 89, 23);
        contentPane.add(btnEliminarTramo);
        
        JLabel btnActualizar = new JLabel("");
        btnActualizar.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		
        		nuevoModelo = actualizarListTramos();
        	
        		listTramos.setModel(nuevoModelo);
        	}
        });
        btnActualizar.setToolTipText("Actualizar Lista");
        btnActualizar.setIcon(new ImageIcon(tramos.class.getResource("/com/sun/javafx/scene/web/skin/Redo_16x16_JFX.png")));
        btnActualizar.setBounds(459, 255, 21, 19);
        contentPane.add(btnActualizar);
        
        lblMensaje2 = new JLabel("");
        lblMensaje2.setBounds(336, 278, 180, 14);
        contentPane.add(lblMensaje2);
        
        listTramos = new JList<NodoTramo>(nuevoModelo);
        listTramos.setBounds(323, 46, 193, 194);
        contentPane.add(listTramos);
        
    }

    protected boolean borrarTramo(NodoTramo tramo) {
		
    	Retorno r = new Retorno();
    	
    	r = app.getSistema().getInstancia().eliminarTramo(tramo);
    	
    	if(r.getResultado() == Resultado.OK){
    		lblMensaje2.setText("Se elimino correctamente!");
    		return true;
    	}else{
    		lblMensaje2.setText("ERROR, no se pudo eliminar verifique");
    		return false;
    	}
    	
	}

	private DefaultListModel<NodoTramo> actualizarListTramos() {
		
		NodoTramo list[] = app.getSistema().getInstancia().getListTramos().getTramos();
		int cantPuntos = app.getCantidadPuntos();
		
		DefaultListModel<NodoTramo> model = new DefaultListModel<>(); 
		
		for(int i = 0; i < cantPuntos; i++){
			
			NodoTramo aux = list[i];
			
			while(aux != null){
				
				model.addElement(aux);
				
				aux = aux.getSiguiente();
			}
		}
		
		return model;
	}

	protected void crearTramo(int peso, String punto1, String punto2) {
		
		Retorno r = new Retorno();
		
		Punto x = app.getSistema().getInstancia().getPuntos().pertenece(punto1).getPunto();
		Punto y = app.getSistema().getInstancia().getPuntos().pertenece(punto2).getPunto();
		
		if(x != null && y != null){
		
			r = app.getSistema().getInstancia().registrarTramo(x.getCoordX(), x.getCoordY(), y.getCoordX(), y.getCoordY(), peso);
		
		}
		
		if(r.getResultado() == Resultado.OK){
			lblMensaje.setText("�Se ingreso correctamente!");
		}else{
			lblMensaje.setText("ERROR al ingresar - �Tramo ya existe!");
		}
	}

	public void cargarPuntos(JComboBox list)
    {
        NodoHash p[] = app.getSistema().getInstancia().listadoPuntos();
        int cant = app.getSistema().getInstancia().get_cantPuntos();
        for(int i = 0; i < cant; i++)
        {
            for(NodoHash raiz = p[i]; raiz != null; raiz = raiz.getDerecha())
                list.addItem(raiz.getPunto().getNombre());

        }

    }
}
