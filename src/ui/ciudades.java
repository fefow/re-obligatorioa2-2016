package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entidades.Retorno;
import entidades.Retorno.Resultado;
import entidades.Sistema;

public class ciudades extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtNombreCiudad;
	private app_config app;
	private JTextField txtCoordX;
	private JTextField txtCoordY;
	private JLabel lblMensaje;
	private JPanel contentPane;


	public ciudades(app_config app) {
		
		this.app = app;
		this.setVisible(true);
		this.setTitle("Obligatorio / Ciudades");
		this.setResizable(false);
		this.setBounds(100, 100, 450, 300);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		contentPane = new JPanel();
		this.setContentPane(contentPane);
		this.contentPane.setLayout(null);
		
		txtNombreCiudad = new JTextField();
		txtNombreCiudad.setToolTipText("Nombre de Ciudad");
		txtNombreCiudad.setBounds(96, 61, 146, 20);
		this.contentPane.add(txtNombreCiudad);
		txtNombreCiudad.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(35, 64, 46, 14);
		this.contentPane.add(lblNombre);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				registrarCiudad(txtNombreCiudad.getText(),Double.parseDouble(txtCoordX.getText()),Double.parseDouble(txtCoordY.getText()));
			}
		});
		btnAgregar.setBounds(96, 185, 89, 23);
		this.contentPane.add(btnAgregar);
		
		JList list = new JList();
		list.setBounds(278, 11, 146, 240);
		this.contentPane.add(list);
		
		lblMensaje = new JLabel("");
		lblMensaje.setFont(new Font("Calibri", Font.PLAIN, 11));
		lblMensaje.setForeground(Color.DARK_GRAY);
		lblMensaje.setBounds(35, 219, 207, 28);
		this.contentPane.add(lblMensaje);
		
		JLabel lblNombreDeLa = new JLabel("Nombre de la Ciudad");
		lblNombreDeLa.setForeground(Color.GRAY);
		lblNombreDeLa.setFont(new Font("Tahoma", Font.ITALIC, 9));
		lblNombreDeLa.setBounds(96, 47, 146, 14);
		this.contentPane.add(lblNombreDeLa);
		
		JLabel lblCoordenaS = new JLabel("Coordena S:");
		lblCoordenaS.setBounds(20, 110, 66, 14);
		this.contentPane.add(lblCoordenaS);
		
		txtCoordX = new JTextField();
		txtCoordX.setBounds(96, 107, 146, 20);
		this.contentPane.add(txtCoordX);
		txtCoordX.setColumns(10);
		
		JLabel lblCoordenadaSur = new JLabel("Coordenada Sur");
		lblCoordenadaSur.setForeground(Color.GRAY);
		lblCoordenadaSur.setFont(new Font("Tahoma", Font.ITALIC, 9));
		lblCoordenadaSur.setBounds(96, 92, 125, 14);
		this.contentPane.add(lblCoordenadaSur);
		
		JLabel lblCoordenadaW = new JLabel("Coordenada W:");
		lblCoordenadaW.setBounds(10, 157, 82, 14);
		this.contentPane.add(lblCoordenadaW);
		
		txtCoordY = new JTextField();
		txtCoordY.setBounds(96, 154, 146, 20);
		this.contentPane.add(txtCoordY);
		txtCoordY.setColumns(10);
		
		JLabel lblCoordenadaOeste = new JLabel("Coordenada Oeste");
		lblCoordenadaOeste.setForeground(Color.GRAY);
		lblCoordenadaOeste.setFont(new Font("Tahoma", Font.ITALIC, 9));
		lblCoordenadaOeste.setBounds(96, 138, 125, 14);
		this.contentPane.add(lblCoordenadaOeste);
	}


	protected void registrarCiudad(String nombre, double coordX, double coordY) {
		
		Retorno r = app.getSistema().getInstancia().registrarCiudad(nombre,coordX,coordY);
		
		if(r.getResultado() == Resultado.OK){
			lblMensaje.setText("�Se agrego ciudad correctamente!");
		}else if(r.getResultado() == Resultado.ERROR_2){
			lblMensaje.setText("�"+ nombre +" ya se encuentra agregada!");
		}else{
			lblMensaje.setText("�Error al ingresar, verifique!");
		}
	}
}
