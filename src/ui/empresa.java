package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import entidades.Retorno;

public class empresa extends JFrame {

	private static final long serialVersionUID = 1L; // no le den bola a este atributo.
	
	private app_config app;
	
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtDireccion;
	private JTextField txtPais;
	private JTextField txtEmail;
	private JTextField txtColor;
	private JLabel lblMensaje;
	

	public empresa(app_config app) { // CONSTRUCTOR
		this.app = app;
		this.setVisible(true);
		this.setTitle("Obligatorio / Empresas");
		this.setResizable(false);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setBounds(100, 100, 490, 530);
		
		contentPane = new JPanel();
		
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRegistrar = new JButton("Registrar");
		
		btnRegistrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				registrarEmpresa(txtNombre.getText(),txtDireccion.getText(),txtPais.getText(),txtEmail.getText(),txtColor.getText());
			}
		});
		
		btnRegistrar.setBounds(104, 268, 77, 30);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(20, 94, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(20, 128, 46, 14);
		contentPane.add(lblDireccion);
		
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(20, 163, 46, 14);
		contentPane.add(lblPais);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(20, 199, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(20, 240, 46, 14);
		contentPane.add(lblColor);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(66, 91, 115, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(66, 125, 115, 20);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		txtPais = new JTextField();
		txtPais.setBounds(66, 160, 115, 20);
		contentPane.add(txtPais);
		txtPais.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(66, 196, 115, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtColor = new JTextField();
		txtColor.setBounds(66, 237, 115, 20);
		contentPane.add(txtColor);
		txtColor.setColumns(10);
		
		JLabel lblRegistroEmpresas = new JLabel("Registro");
		lblRegistroEmpresas.setFont(new Font("Arial", Font.PLAIN, 19));
		lblRegistroEmpresas.setBounds(20, 60, 72, 23);
		contentPane.add(lblRegistroEmpresas);
		
		lblMensaje = new JLabel("");
		lblMensaje.setIcon(null);
		lblMensaje.setBounds(20, 352, 171, 48);
		contentPane.add(lblMensaje);
		
		JLabel lblEmpresas = new JLabel("Empresas");
		lblEmpresas.setFont(new Font("Arial", Font.PLAIN, 26));
		lblEmpresas.setBounds(10, 11, 171, 44);
		contentPane.add(lblEmpresas);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				eliminarEmpresa(txtNombre.getText());
			}
		});
		btnEliminar.setBounds(20, 268, 77, 30);
		contentPane.add(btnEliminar);

	}
	
	
	private void registrarEmpresa(String nombre, String direccion, String pais, String email, String color) {

		Retorno r = app.getSistema().getInstancia().registrarEmpresa(nombre, direccion, pais, email, color);
		
		if(r.getResultado() == Retorno.Resultado.OK){
			lblMensaje.setText("Se agrego correctamente");
		}else{
			lblMensaje.setText("Error al ingresar, verifique.");
		}
	}
	
	private void eliminarEmpresa(String nombreEmpresa) {
		
		//Retorno r = app.getInstancia().getSistema().eliminarEmpresa(nombreEmpresa);
				
	}
}
