package ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import entidades.Retorno;

public class main {

	private JFrame frame;
	private static app_config app;
	private AbrirUrl mapa;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public main(){ // Constructor
		
		iniciarHash();
		
		initialize();

	}
	
	public void iniciarHash(){
		/*
		 * Funcion al pedo para setear la cantidad de puntos que van a ser los DC y las ciudades */
		
		int puntos = -1;

		while(puntos < 0){
			puntos =  Integer.parseInt(JOptionPane.showInputDialog("Ingrese numero de puntos: "));
		}
		
		app = new app_config(puntos);
		
	
	}
	

	private void initialize() {
		
		cargarDatos();
		// seteando el frame
		
		frame = new JFrame();
	
		frame.setBounds(100, 100, 900, 650);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		
		// fin seteo de frame 
		
		// Menu 
		
		JMenuBar menuBar = new JMenuBar();
		
		menuBar.setBounds(0, 0, 900, 40);
		
		frame.getContentPane().add(menuBar);
		
		JMenu mnEmpresas = new JMenu("Empresas");
		
		JMenu mnCiudades = new JMenu("Ciudades");
		
		JMenu mnTramos = new JMenu("Tramos");
		
		menuBar.add(mnEmpresas);
		menuBar.add(mnCiudades);
		menuBar.add(mnTramos);
		
		// Fin Menu
		
		// Agregando items al menu 
		
		JMenuItem mntmAgregar = new JMenuItem("Agregar");
		
		mntmAgregar.addActionListener(new ActionListener() {
			
			// funcion del boton Agregar en el menu empresas
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				empresa emp = new empresa(app); // crea un nuevo Jframe empresa.
											 // en el consturctor de empresa
			}								 // esta seteado el jframe.
		
		});
		
		
		JMenuItem mntmPanel = new JMenuItem("Panel");
		
		mntmPanel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg1) {
			
				ciudades ciudades = new ciudades(app);
			}	
		});
		
		JMenuItem mntmAgregarTramo = new JMenuItem("Agregar");
		
		mntmAgregarTramo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg1) {
			
				tramos window = new tramos(app);
			}	
		});
		

		mnEmpresas.add(mntmAgregar);
		
		mnCiudades.add(mntmPanel);
		
		mnTramos.add(mntmAgregarTramo);
		
		// Fin Items del menu
		
		JButton btnMostrar = new JButton("Mostrar mapa");
		btnMostrar.setBounds(100, 100, 150, 60);
		frame.add(btnMostrar);
		
		btnMostrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				verMapa();
			}
		});
	}
	

	private void verMapa(){
		Retorno r = app.getSistema().getInstancia().mapaEstado();
		
		AbrirUrl.openURL(r.getValorString());
	}

	public AbrirUrl getMapa() {
		return mapa;
	}

	public void setMapa(AbrirUrl mapa) {
		this.mapa = mapa;
	}
	
	public void cargarDatos(){
		
		app.getSistema().getInstancia().registrarEmpresa("ANTEL", "tu vieja", "Uruguay", "antel@vera.com.uy", "red");
		app.getSistema().getInstancia().registrarEmpresa("MOVISTAR", "arrayanes 2152", "Uruguay", "contacto@movistar.com.uy", "blue");
		app.getSistema().getInstancia().registrarCiudad("Montevideo", -34.81958, -56.37029);
		app.getSistema().getInstancia().registrarCiudad("Maldonado", -34.90404, -54.97569);
		app.getSistema().getInstancia().registrarCiudad("Rocha", -34.47819, -54.33436);
		app.getSistema().getInstancia().registrarCiudad("Minas", -34.37334, -55.24792);
		app.getSistema().getInstancia().registrarCiudad("Canelones", -34.52560, -56.30399);
		app.getSistema().getInstancia().registrarDC("DataCenter1", -34.52560, -54.30399,"ANTEL", 123, 1500);	
		app.getSistema().getInstancia().registrarDC("DataCenter2", -31.82560, -56.30399,"ANTEL", 100, 3500);
		app.getSistema().getInstancia().registrarDC("DataCenter3", -32.81958, -55.37029,"MOVISTAR",100, 9000);

		app.getSistema().getInstancia().eliminarPunto(-34.37334, -55.24792);
		
		app.getSistema().getInstancia().registrarTramo(-34.81958, -56.37029, -34.90404, -54.97569, 20);
	
	}

}
