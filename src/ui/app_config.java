package ui;

import entidades.Sistema;

public class app_config {

	private int cantidadPuntos;
	
	private Sistema sistema;
	
	public app_config(int puntos) {
		sistema = new Sistema(puntos);
		cantidadPuntos = puntos;
	}

	public Sistema getSistema() {
		return sistema;
	}

	public int getCantidadPuntos() {
		return cantidadPuntos;
	}

	public void setCantidadPuntos(int cantidadPuntos) {
		this.cantidadPuntos = cantidadPuntos;
	}
}
