package entidades;

import interfaces.IPunto;

public abstract class Punto implements IPunto {
	
	/*
	 * Los data center y las ciudades heredan de esta clase, asi los podemos tratar a los 2
	 * como puntos y tener una tabla hasha de puntos.
	 */
	
	private Double coordX;
	private Double coordY;
	private TipoPunto tipo;
	
	public Double getCoordX() {
		return coordX;
	}
	public void setCoordX(Double coordX) {
		this.coordX = coordX;
	}
	public TipoPunto getTipo() {
		return tipo;
	}
	public void setTipo(TipoPunto tipo) {
		this.tipo = tipo;
	}
	public Double getCoordY() {
		return coordY;
	}
	public void setCoordY(Double coordY) {
		this.coordY = coordY;
	}
	
	public String getCoordenadas() {
		   
    	return coordX + "," + coordY;
    
    }

	
}
