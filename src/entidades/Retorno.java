package entidades;

public class Retorno {

	public enum Resultado{OK, ERROR_1, ERROR_2, ERROR_3, ERROR_4, ERROR_5, NO_IMPLEMENTADA};

	private int valorEntero;
	private String valorString;
	private Resultado resultado;
	private Boolean valorBooleano;
	
	public Retorno() {

		this.resultado = Resultado.NO_IMPLEMENTADA;
		this.valorString = "";
		this.valorEntero = 0;
		this.valorBooleano = false;
	}

	public Retorno(Resultado resultado) {

		this.resultado = resultado;

	}

	public Retorno(Resultado resultado, String valorstr, int valorEnt, Boolean b) {

		this.resultado = resultado;

		this.valorEntero = valorEnt;

		this.valorString = valorstr;
		
		this.valorBooleano = b;

	}

	public int getValorEntero() {

		return valorEntero;

	}

	public void setValorEntero(int valorEntero) {

		this.valorEntero = valorEntero;

	}

	public String getValorString() {

		return valorString;

	}

	public void setValorString(String valorString) {

		this.valorString = valorString;

	}


	public Resultado getResultado() {

		return resultado;

	}

	public void setResultado(Resultado resultado) {

		this.resultado = resultado;

	}

	public Boolean getValorBooleano() {
		return valorBooleano;
	}

	public void setValorBooleano(Boolean valorBooleano) {
		this.valorBooleano = valorBooleano;
	}

}
