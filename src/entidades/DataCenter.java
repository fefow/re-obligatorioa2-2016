package entidades;

public class DataCenter extends Punto {

	private String nombre;
	private Empresa Empresa;
	private int capacidadCPUenHoras;
	private int costoCPUporHora;
	
	public DataCenter(String nombre, Double coordX, Double coordY, Empresa Empresa, int capacidadCPUenHoras, int costoCPUporHora){
	
		this.nombre = nombre;
		this.Empresa = Empresa;
		this.capacidadCPUenHoras = capacidadCPUenHoras;
		this.costoCPUporHora = costoCPUporHora;
		this.setCoordX(coordX);
		this.setCoordY(coordY);
		this.setTipo(TipoPunto.DATACENTER);
		
	}

	@Override
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Empresa getEmpresa() {
		return Empresa;
	}

	public void setEmpresa(Empresa empresa) {
		Empresa = empresa;
	}

	public int getCapacidadCPUenHoras() {
		return capacidadCPUenHoras;
	}

	public void setCapacidadCPUenHoras(int capacidadCPUenHoras) {
		this.capacidadCPUenHoras = capacidadCPUenHoras;
	}

	public int getCostoCPUporHora() {
		return costoCPUporHora;
	}

	public void setCostoCPUporHora(int costoCPUporHora) {
		this.costoCPUporHora = costoCPUporHora;
	}
	
	public String toString() {
		return nombre;
	};
}
