package entidades;

public class ResultadoBusquedaABB {
	
	/*
	 * Esta clase solo esta d ayuda para reubicar los hijos a la hora d borrar un nodo
	 * del arbol, asi podemos re ubicar los nodos hijos a partir de su padre.
	 */
	
	private NodoArbol nodoPadre;
	
	private NodoArbol nodo;
	
	public ResultadoBusquedaABB() {
	
	}
	
	public NodoArbol getNodoPadre() {
		return nodoPadre;
	}
	public void setNodoPadre(NodoArbol nodoPadre) {
		this.nodoPadre = nodoPadre;
	}
	public NodoArbol getNodo() {
		return nodo;
	}
	public void setNodo(NodoArbol nodo) {
		this.nodo = nodo;
	}
}
