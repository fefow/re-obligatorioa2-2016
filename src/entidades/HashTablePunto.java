package entidades;

import entidades.Retorno.Resultado;

public class HashTablePunto {

	private int largoArray;
	private static int ids = 1;
	private NodoHash[] puntos;
	
	public HashTablePunto(int cant) { // CONSTRUCTOR
		
		this.largoArray = cant;
		this.puntos = new NodoHash[largoArray];
		
	}
	
	private int getHashCode(String nombre, int largoArray){

		int disp = 0;   

		for (int i = 0; i < nombre.length(); i++) {

			disp += nombre.charAt(i);

		}

		return disp % largoArray;
		
	}

	public Retorno insertar(Punto punto){
		
		Retorno r = new Retorno();

		NodoHash aux = pertenece(punto.getNombre());
		
		if(aux == null){
			
			aux = new NodoHash(darProximoId(),punto);
			
			insertarNodo(aux);
			
			r.setResultado(Resultado.OK);
			
			
		}else{
			r.setResultado(Resultado.ERROR_2);
		}
		
		return r;
	}

	private static int darProximoId() {
		int a = ids++;
		return a;
	}

	private void insertarNodo(NodoHash nodo) {
		
		int lugar = getHashCode(nodo.getPunto().getNombre(),this.largoArray);
		
		if(puntos[lugar] == null){
			
			puntos[lugar] = nodo;
		
		}else{
			
			NodoHash aux = encontrarNodoApuntandoANull(puntos[lugar]);
			aux.setDerecha(nodo);
			nodo.setIzquierda(aux);
		}
	}

	private NodoHash encontrarNodoApuntandoANull(NodoHash raiz) {
		
		if(raiz.getDerecha() == null){
			
			return raiz;
		
		}else{
			
			return encontrarNodoApuntandoANull(raiz.getDerecha());
		
		}
	
	}

	public NodoHash pertenece(String nombre) {

		int numeroHash = getHashCode(nombre,largoArray);
		
		NodoHash aux = null;
		
		NodoHash raiz = puntos[numeroHash];
		
		int cont = 0;
		
		if(raiz != null){
			
			while(cont == 0){
				
				if(raiz.getPunto().getNombre().equals(nombre)){
					
					aux = raiz;
					
					cont = 1;
				
				}else{
					
					if(raiz.getDerecha() != null){
						
						raiz = raiz.getDerecha();
					
					}else{
						
						cont = 1;
					
					}
				}
			}
		}
		
		return aux;
		
	}
	
	public NodoHash pertenece(Double x, Double y){
		
		NodoHash aux = new NodoHash();
		
		int cont = 0;
		
		for(int i = 0; i < largoArray; i++){
			
			if(cont != 1){
				
				NodoHash raiz = puntos[i];
					
				if(raiz != null){
					
					while(raiz != null){
						
						int coordX = Double.compare(raiz.getPunto().getCoordX(), x);
						int coordY = Double.compare(raiz.getPunto().getCoordY(), y);
						
						if( coordX == 0 && coordY == 0){
						
							aux = raiz;
							
							cont = 1;
							
							raiz = null;
							
						}else{
							
							if(raiz.getDerecha() != null){
								
								raiz = raiz.getDerecha();
							
							}else{
							
								raiz = null;
							}
					    }
						
					  }	
				   }
					
			}
		}
		
		return aux;
	}

	public Retorno borrar(Punto punto){
		
		Retorno r = new Retorno();
		
		NodoHash aux = pertenece(punto.getNombre());
		
		if(aux != null){

			if(aux.getIzquierda() != null){

				acomodarNodosConHijosIzq(aux);

			}else{
				
				if(aux.getDerecha() != null){
					
					NodoHash bux = aux.getDerecha();
					
					bux.setIzquierda();
				
				}
			}
			
			aux = null;
			
			
			r.setResultado(Resultado.OK);
			
		}else{
			
			r.setResultado(Resultado.ERROR_1);

		}

		return r;
	}

	public Retorno borrarPorCoordenadas(Double x, Double y){
		
		Retorno r = new Retorno(Resultado.ERROR_1);
		
		int cont = 0;
		
		for(int i = 0; i < largoArray; i++){
			
			if(cont != 1){
				
			NodoHash raiz = puntos[i];
				
			if(raiz != null){
				
				while(raiz != null){
					
					int coordX = Double.compare(raiz.getPunto().getCoordX(), x);
					int coordY = Double.compare(raiz.getPunto().getCoordY(), y);
					
					if( coordX == 0 && coordY == 0){
						
						if(raiz.getIzquierda() != null){
	
							acomodarNodosConHijosIzq(raiz);
	
						}else{
							
							if(raiz.getDerecha() != null){
								
								NodoHash bux = raiz.getDerecha();
								
								bux.setIzquierda();
							
							}
						}
						
						raiz = null;
						
						cont = 1;
						
						r.setResultado(Resultado.OK);
						
					}else{
						if(raiz.getDerecha() != null){
							raiz = raiz.getDerecha();
						}else{
							raiz = null;
						}
					}
				 }
				
				}
				
			}
		}
		
		return r;
	}
	
	private void acomodarNodosConHijosIzq(NodoHash nodo) {
		
		NodoHash izq = new NodoHash();
		
		if(nodo.getIzquierda() != null){
			izq = nodo.getIzquierda();
		}
		
		izq.setDerecha(nodo.getDerecha());

	}

	public Retorno esVacio(NodoHash nodo){
		
		Retorno r = new Retorno();
		
		if(nodo != null){
			r.setValorBooleano(true);
		}
		
		return r;
	}
	
	public int cantidadDePuntosIngresados() {

		int cont = 0;

		for (int x = 0; x < largoArray; x++) {

			NodoHash raiz = puntos[x];

			while (raiz != null) {

				cont++;
				raiz = raiz.getDerecha();

			}
		}

		return cont;
	}
	
	public int tamanioArray() {

		return this.largoArray;
	}

	public NodoHash[] getPuntos() {
		return puntos;
	}

	public void setPuntos(NodoHash[] puntos) {
		this.puntos = puntos;
	}

	
}
