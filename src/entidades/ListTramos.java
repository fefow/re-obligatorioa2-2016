package entidades;

import entidades.Retorno.Resultado;

public class ListTramos {
	
	private int cpuntos;
	private int dpuntos;
	private NodoTramo tramos[];
	
	public ListTramos(int cantidad_puntos){ // CONSTRUCTOR
		this.cpuntos = cantidad_puntos;
		this.dpuntos = 0;
		this.tramos = new NodoTramo[cantidad_puntos];
	}

	public Retorno agregarTramo(Punto x, Punto y, int peso) {

		Retorno r = new Retorno(Resultado.ERROR_1);
	
		NodoTramo aux = new NodoTramo(peso, x, y);
		
		NodoTramo c = buscarTramo(x);
		
		if( verificarSiYaExiste(aux) == false){
		
			if(c == null){
				
				if (dpuntos <= cpuntos) {
					
					tramos[dpuntos] = aux;
		
					this.dpuntos = dpuntos + 1;
					
					r.setResultado(Resultado.OK);
				}
				
			}else{
				
				c.setSiguiente(aux);
				
				r.setResultado(Resultado.OK);
			}
		}

		return r;

	}

	
	public NodoTramo buscarTramo(Punto aux){
		
		NodoTramo tramo = null;
		int b = 0;
		
		for(int i = 0; i < this.cpuntos; i++){
			
			if(b != 1){
				
				NodoTramo bux = tramos[i];
			
				while(bux != null){
					
					if(bux.getPuntoX().getNombre().equals(aux.getNombre())){
						
						tramo = bux;
						b = 1;
						
					}
						
					bux = bux.getSiguiente();
					
					
				}
			}
		}
		
		return tramo;
		
	}

	private boolean verificarSiYaExiste(NodoTramo aux) {
		
		boolean r = false;
		int b = 0;
		
		for(int i = 0; i < this.cpuntos; i++){
		
			if(b != 1){
				
				NodoTramo bux = tramos[i];
			
			
				while(bux != null){
					
					if(comprarSiSonIgualesNodoTramos(aux,bux)== true){
						
						r = true;	
						b = 1;
					}
					
					bux = bux.getSiguiente();
					
				}
			}
		}
		
		return r;
	}



	private boolean comprarSiSonIgualesNodoTramos(NodoTramo aux, NodoTramo bux) {
		
		if(aux.getPuntoX().getNombre() == bux.getPuntoX().getNombre()){
			if(aux.getPuntoY().getNombre() == bux.getPuntoY().getNombre()){
				return true;
			}
		}

		return false;
	}
	
	public Retorno eliminar(NodoTramo tramo){
		
		Retorno r = new Retorno(Resultado.ERROR_1);
		int b = 0;
		NodoTramo TramoAnterior = null;
		
		for(int i = 0; i < this.cpuntos; i++){
			
			if(b != 1){
				
				NodoTramo bux = tramos[i];
			
				while(bux != null){
					
					if(comprarSiSonIgualesNodoTramos(bux,tramo)){
						
						if(TramoAnterior != null){
							TramoAnterior.setSiguiente(bux.getSiguiente());
							tramos[i] = null;
						}
						
						b = 1;
						bux = null;
						r.setResultado(Resultado.OK);
						
					}else{
						
						if(bux != null)
							TramoAnterior = bux;
							bux = bux.getSiguiente();
						}
					}
			}
		}
		
		
		return r;
	}
	

	public NodoTramo[] getTramos() {
		return tramos;
	}

	public void setTramos(NodoTramo tramos[]) {
		this.tramos = tramos;
	}

	public int getDpuntos() {
		return dpuntos;
	}

	public void setDpuntos(int dpuntos) {
		this.dpuntos = dpuntos;
	}

}
