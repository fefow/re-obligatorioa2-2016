package entidades;

public class NodoArbol {
	
	private Empresa empresa;
	private NodoArbol hijoDerecho;
	private NodoArbol hijoIzquierdo;
	
	public NodoArbol() {
		this.empresa = null;
		this.hijoDerecho = null;
		this.hijoIzquierdo = null;
	}
	
	public NodoArbol(Empresa emp){
		this.empresa = emp;
		this.hijoDerecho = null;
		this.hijoIzquierdo = null;
	}

	public NodoArbol getHijoDerecho() {
		return hijoDerecho;
	}

	public void setHijoDerecho(NodoArbol hijoDerecho) {
		this.hijoDerecho = hijoDerecho;
	}

	public NodoArbol getHijoIzquierdo() {
		return hijoIzquierdo;
	}

	public void setHijoIzquierdo(NodoArbol hijoizquierdo) {
		this.hijoIzquierdo = hijoizquierdo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
