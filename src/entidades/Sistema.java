package entidades;
import entidades.Retorno.Resultado;
import interfaces.ISistema;

public class Sistema implements ISistema {
	
	private int _cantPuntos;
	private HashTablePunto puntos;
	private ABB arbolEmpresas;
	private Mapa map;
	private ListTramos tramos;
	private Sistema instancia;
	
	public Sistema getInstancia(){
		
		if(instancia == null){
			instancia = new Sistema(get_cantPuntos());
		}
		return instancia;
	}
	
	public Sistema(int puntos) {
		inicilizarSistema(puntos);
	}
	
	@Override
	public Retorno inicilizarSistema(int cantPuntos) {
		
		this._cantPuntos = cantPuntos;
		this.puntos = new HashTablePunto(cantPuntos);
		this.arbolEmpresas = new ABB();
		this.tramos = new ListTramos(cantPuntos);
		
		return (cantPuntos > 0) ? new Retorno(Retorno.Resultado.OK) : new Retorno(Retorno.Resultado.ERROR_1);
		//si cantPunto > 0 retorna OK, de lo contrario retorna ERROR_1
	}

	@Override
	public Retorno destruirSistema() {
		
		this.puntos = null;
		this.arbolEmpresas = null;
		this._cantPuntos = 0;
		this.tramos = null;
		
		return new Retorno(Resultado.OK);
	}

	@Override
	public Retorno registrarEmpresa(String nombre, String direccion, String pais, String email_contacto, String color) {
		
		Retorno res = this.arbolEmpresas.agregarNodo(nombre, direccion, pais, email_contacto, color);
			
		return res;
	}

	@Override
	public Retorno registrarCiudad(String nombre, Double coordX, Double coordY) {
		
		Punto ciudad = new Ciudad(nombre,coordX,coordY);
		
		ciudad.setTipo(TipoPunto.CIUDAD);
		
		Retorno r = new Retorno();
		
		r.setResultado(Resultado.ERROR_1);
		
		r = this.puntos.insertar(ciudad);
		
		return r;
	}

	@Override
	public Retorno registrarDC(String nombre, Double coordX, Double coordY, String Empresa, int capacidadCPUenHoras,
			int costoCPUporHora) {
		
		Empresa empresaAux = arbolEmpresas.buscarNodo(Empresa, arbolEmpresas.getRaiz()).getEmpresa();
		
		Retorno res = new Retorno();
		
		if(empresaAux != null){
			
			Punto p = new DataCenter(nombre,coordX,coordY,empresaAux,capacidadCPUenHoras,costoCPUporHora);
			
			res = this.puntos.insertar(p);
		
		}else{
			res.setResultado(Resultado.ERROR_3);
		}
    	
		return res;
	}

	@Override
	public Retorno registrarTramo(Double coordXi, Double coordYi, Double coordXf, Double coordYf, int peso) {

		Retorno r = new Retorno();
		
		//comprobamos que existan los puntos ingresados.
		Punto uno = puntos.pertenece(coordXi, coordYi).getPunto();
		Punto dos = puntos.pertenece(coordXf, coordYf).getPunto();
		
		if(uno != null && dos != null){
			r = tramos.agregarTramo(uno,dos,peso);
		}
	
		return r;
	}

	@Override
	public Retorno eliminarTramo(NodoTramo t) {
		
		Retorno r = new Retorno();
		
		if(t != null){
			r = tramos.eliminar(t); 
		}else{
			r.setResultado(Resultado.ERROR_1);
		}
		
		return r;
	}

	@Override
	public Retorno eliminarPunto(Double coordX, Double coordY) {
		
		Retorno r = new Retorno(Resultado.OK);
		
		r = puntos.borrarPorCoordenadas(coordX,coordY);
		
		return r;
	}

	@Override
	public Retorno mapaEstado() {
		
		map = new Mapa(this.puntos,this.arbolEmpresas);
		
		Retorno r =  map.GenerarReportMapa();
		
		return r;
		
	}

	@Override
	public Retorno procesarInformacion(Double coordX, Double coordY, int esfuerzoCPUrequeriendoEnHora) {
		
		return new Retorno(Resultado.NO_IMPLEMENTADA);
	}

	@Override
	public Retorno listadoRedMinima() {
		
		return new Retorno(Resultado.NO_IMPLEMENTADA);
	}

	@Override
	public Retorno listadoEmpresas() {
		
		return new Retorno(Resultado.NO_IMPLEMENTADA);
	}

	public ABB getArbolEmpresas() {
		return arbolEmpresas;
	}

	public void setArbolEmpresas(ABB arbolEmpresas) {
		this.arbolEmpresas = arbolEmpresas;
	}

	public HashTablePunto getPuntos() {
		return puntos;
	}

	public void setPuntos(HashTablePunto puntos) {
		this.puntos = puntos;
	}

	public int get_cantPuntos() {
		return _cantPuntos;
	}

	public void set_cantPuntos(int _cantPuntos) {
		this._cantPuntos = _cantPuntos;
	}

	public ListTramos getListTramos() {
		return tramos;
	}

	public void setListTramos(ListTramos tramos) {
		this.tramos = tramos;
	}

	public NodoHash[] listadoPuntos(){
        NodoHash aux[] = puntos.getPuntos();
        return aux;
    }

}
