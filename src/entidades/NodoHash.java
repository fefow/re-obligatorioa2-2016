package entidades;

public class NodoHash {
	
	private int id;
	private Punto punto;
	private NodoHash izquierda;
	private NodoHash derecha;
	
	//Constructor Ciudad 1
	public NodoHash(int id, Punto punto) {
		this.id = id;
		this.punto = punto;
		this.izquierda = null;
		this.derecha = null;
	}
	
	//Constructor Ciudad 2
	public NodoHash(int id, Punto punto, NodoHash izq, NodoHash der) {
		this.id = id;
		this.punto = punto;
		this.izquierda = izq;
		this.derecha = der;
	}
	
	public NodoHash() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public NodoHash getIzquierda() {
		return izquierda;
	}
	public void setIzquierda(NodoHash izquierda) {
		this.izquierda = izquierda;
	}
	public void setIzquierda() {
		this.izquierda = null;
	}
	public NodoHash getDerecha() {
		return derecha;
	}
	public void setDerecha(NodoHash derecha) {
		this.derecha = derecha;
	}

	public Punto getPunto() {
		return punto;
	}

	public void setPunto(Punto punto) {
		this.punto = punto;
	}
}
