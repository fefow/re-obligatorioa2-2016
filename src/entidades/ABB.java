package entidades;

import entidades.Retorno.Resultado;

public class ABB { // Arbol binario de Busqueda
	
	private NodoArbol raiz;

	public ABB() { // CONSTRUCTOR
		raiz = null;
	}
	
	public Retorno agregarNodo(String nombre, String direccion, String pais, String email_contacto, String color){
		
		Retorno resultado = new Retorno();
		
		NodoArbol aux = buscarNodo(nombre, this.raiz);
		
		if(aux == null){
			
			Empresa emp = new Empresa(nombre, direccion, pais, email_contacto, color);
			
			aux = new NodoArbol(emp); // reutilizo la funcion d arriba, pero c puede definir otra
			
			agregarNodoAlArbol(aux, this.raiz);
			
			resultado.setResultado(Resultado.OK);
			
		}else{
			
			resultado.setResultado(Resultado.ERROR_1);
		}
		
		return resultado;
	}
	
	private void agregarNodoAlArbol(NodoArbol nuevo, NodoArbol nodoActual) {
		 
    	//	 
    	//	Se utiliza la funcion compareTo para comprar Strings.
    	//  1 = si es mayor | 0 = si es igual | -1 = es menor 
    	//
    	
    	if(esVacio()){ // paso base
    		
    		this.raiz = nuevo;
    		
    	}else{
    		
    		// Primero pregunta si es mayor
    		
    		if(nuevo.getEmpresa().getNombre().compareToIgnoreCase(nodoActual.getEmpresa().getNombre()) > 0){
    			
    			if( nodoActual.getHijoDerecho() == null){
    				
    				nodoActual.setHijoDerecho( nuevo );
    			
    			}else if( nodoActual.getHijoDerecho().getEmpresa().getNombre().compareToIgnoreCase(nuevo.getEmpresa().getNombre()) == 1){
    				
    				nuevo.setHijoDerecho(nodoActual.getHijoDerecho());
    				
    				nodoActual.setHijoDerecho(nuevo);
    				
    			}else{
    				
    				agregarNodoAlArbol(nuevo,nodoActual.getHijoDerecho());
    				
    			}
    			
    		}else{
    			
    			// Despues si es menor
    			
    			if( nodoActual.getHijoIzquierdo() == null){
    				
    				nodoActual.setHijoIzquierdo( nuevo );
    			
    			}else if( nodoActual.getHijoIzquierdo().getEmpresa().getNombre().compareToIgnoreCase(nuevo.getEmpresa().getNombre()) >= 0 ){
    				
    				nuevo.setHijoIzquierdo(nodoActual.getHijoIzquierdo());
    				
    				nodoActual.setHijoIzquierdo(nuevo);
    				
    			}else{
    				
    				agregarNodoAlArbol(nuevo,nodoActual.getHijoIzquierdo());
    				
    			}
    			
    		}
    		
    	}

	}

	public boolean esVacio() {

		boolean r = false;
		
		if(raiz == null){
			r = true;
		}
		
        return r;

    }

	public NodoArbol buscarNodo(String nombre, NodoArbol raiz) {
		
		return buscarNodoConPadre(nombre, raiz, null).getNodo();
	
	}

	private ResultadoBusquedaABB buscarNodoConPadre(String nombre, NodoArbol raiz, NodoArbol padre) {
		
		/* CompareToIgnoreCase
		* Si da 
		* 1 = es mayor
		* 0 = es igual
		* -1 = es menor
		*/
		
		ResultadoBusquedaABB resultado = new ResultadoBusquedaABB();
		
		if(raiz == null){
			return resultado;
		}else{
			if(raiz.getEmpresa().getNombre().equals(nombre)){
				resultado.setNodo(raiz);
				resultado.setNodoPadre(padre);
			}else{
				
				int comparacionDeNombres = (nombre).compareToIgnoreCase(raiz.getEmpresa().getNombre());
				
				if( comparacionDeNombres > 1){
					resultado = buscarNodoConPadre(nombre, raiz.getHijoDerecho(), raiz);
				}else{
					resultado = buscarNodoConPadre(nombre, raiz.getHijoIzquierdo(), raiz);
				}
			}
		}
		
		return resultado;
	}
	
	public Retorno eliminarNodo(String nombre) {
    	
    	Retorno retorno = new Retorno();
    	
    	ResultadoBusquedaABB nodoYpadre = buscarNodoConPadre(nombre, this.raiz, null);
    	
    	if(nodoYpadre.getNodo() != null){
    		
    		ABB hijos = new ABB();
    		
    		listarHijosNodo(nodoYpadre.getNodo().getHijoDerecho(),hijos);
    		
    		listarHijosNodo(nodoYpadre.getNodo().getHijoIzquierdo(),hijos);
    		
    		quitarNodoDelPadre(nodoYpadre.getNodo(), nodoYpadre.getNodoPadre());
    		
    		reubicarHijoAlArbol(hijos, nodoYpadre.getNodoPadre());
    		
    		retorno.setResultado(Resultado.OK);
    		
    	}else{
    		
    		retorno.setResultado(Resultado.ERROR_1);
    		
    	}
    	
    	return retorno;
    }

	private void reubicarHijoAlArbol(ABB hijos, NodoArbol nodoPadre) {
		
		NodoArbol raizActual = this.raiz;
    	
    	if( nodoPadre != null){
    		
    		raizActual = nodoPadre;
    	
    	}
	    	while(nodoPadre.getHijoDerecho() != null || nodoPadre.getHijoIzquierdo() != null){
	    		
	    		if(nodoPadre.getHijoDerecho() !=null){
	    			agregarNodoAlArbol(nodoPadre.getHijoDerecho(), raizActual);
	    		}
	    		if(nodoPadre.getHijoIzquierdo() !=null){
	    			agregarNodoAlArbol(nodoPadre.getHijoIzquierdo(), raizActual);
	    		}
	    		
	    	}
		
	}

	private void quitarNodoDelPadre(NodoArbol nodo, NodoArbol nodoPadre) {
		
		if(nodoPadre == null){
			
			this.raiz = null;
			
		}else{
		
			if(nodoPadre.getHijoDerecho().equals(nodo)){
				
				nodoPadre.setHijoDerecho(null);	
				
			}else{
				
				nodoPadre.setHijoIzquierdo(null);	
				
			}
		}
	}

	public void listarHijosNodo(NodoArbol nodo, ABB hijos){
    	
		if(hijos.getRaiz() == null){
			
			hijos.setRaiz(nodo);
		
		}else{
		
	    	if(nodo != null){
	    		
	    		hijos.agregarNodoAlArbol(nodo, hijos.getRaiz());
	    		
	    		if(nodo.getHijoDerecho() != null){
	    			
	    			listarHijosNodo(nodo.getHijoDerecho(),hijos);
	    			
	    		}
	    		
	    		if(nodo.getHijoIzquierdo() != null){
	    			
	    			listarHijosNodo(nodo.getHijoIzquierdo(), hijos);
	    			
	    		}
	    	}
		}
    	
    }
	
	public String returnColor(String empresa){
		
		NodoArbol aux = buscarNodo(empresa,this.raiz);
		String color = "white";
		
		if(aux != null){
			color = aux.getEmpresa().getColor();
		}
		
		return color;
	}

	
	public NodoArbol getRaiz() {
		return raiz;
	}

	public void setRaiz(NodoArbol raiz) {
		this.raiz = raiz;
	}
	
}
