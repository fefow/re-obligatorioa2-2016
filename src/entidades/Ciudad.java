package entidades;

public class Ciudad extends Punto {
	
	private String nombre;

	public Ciudad(String nombre, Double coordX, Double coordY) {
		this.nombre = nombre;
		this.setCoordX(coordX);
		this.setCoordY(coordY);
		this.setTipo(TipoPunto.CIUDAD);
	}
	
	public Ciudad(String nombre, Punto punto) {
		this.nombre = nombre;
	}
	
	public Ciudad(String nom) {
		this.nombre = nom;
	}
	
	@Override
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}

	@Override
	public Empresa getEmpresa() {
		return null;
	}
}
