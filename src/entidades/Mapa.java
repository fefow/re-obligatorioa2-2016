package entidades;

import entidades.Retorno.Resultado;

public class Mapa {
	/*/
	 * Aca van a ir todas las funciones relacionadas al mapa.
	 */
	private HashTablePunto puntos;
	private ABB empresas;
	private String urlBase = "http://maps.googleapis.com/maps/api/staticmap?center=Uruguay&zoom=7&size=1200x800&maptype=roadmap";
	
	
	public Mapa(HashTablePunto p, ABB e) {

		empresas = e;
		puntos = p;
		
	}
	
	
	public Retorno GenerarReportMapa(){
		
		Retorno resultado = new Retorno();
		
		String urlMapaGenerado = urlBase + procesarPuntos();
		
		resultado.setValorString(urlMapaGenerado);
		
		resultado.setResultado(Resultado.OK);
		
		return resultado;
	}
	
	public String procesarPuntos(){
		
		NodoHash array_puntos[] = puntos.getPuntos();
		
		String puntosString = "";
		
		int cont = 0;
		
		NodoHash raiz = new NodoHash();
		
		int tamanio = puntos.tamanioArray();
		
		for(int i = 0; i < tamanio ; i++ ){
			
			raiz = array_puntos[i];
			
			while(raiz != null){
				
				if(raiz.getPunto().getTipo() == TipoPunto.CIUDAD){
					
					puntosString += generarPuntoCiudad(raiz);
				
				}else{
					
					puntosString += generarPuntoDataCenter(raiz);
				
				}
				
				raiz = raiz.getDerecha();
				
				cont++;
			
			}
		}
			
		return puntosString;
	}
	
	private String generarPuntoDataCenter(NodoHash raiz) {
		
		String color = empresas.returnColor(raiz.getPunto().getEmpresa().getNombre());
		
		return "&markers=color:"+ color  +"|label:D|"+ raiz.getPunto().getCoordenadas();
		
	}


	private String generarPuntoCiudad(NodoHash raiz) {
		return "&markers=color:yellow|label:C|"+ raiz.getPunto().getCoordenadas();
	}
}
