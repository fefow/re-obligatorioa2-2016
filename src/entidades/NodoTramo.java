package entidades;

public class NodoTramo {
		
	private int peso;
	private Punto puntoX;
	private Punto puntoY;
	private NodoTramo siguiente;
	
	public NodoTramo() {
		
	}
	
	public NodoTramo(int peso, Punto x, Punto y){
		this.peso = peso;
		this.puntoX = x;
		this.puntoY = y;
		this.siguiente = null;
	}
	
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	public Punto getPuntoX() {
		return puntoX;
	}
	public void setPuntoX(Punto puntoX) {
		this.puntoX = puntoX;
	}
	public Punto getPuntoY() {
		return puntoY;
	}
	public void setPuntoY(Punto puntoY) {
		this.puntoY = puntoY;
	}
	public NodoTramo getSiguiente() {
		return siguiente;
	}
	public void setSiguiente(NodoTramo siguiente) {
		this.siguiente = siguiente;
	}
	@Override
	public String toString() {
		return puntoX.getNombre() + " -> " + puntoY.getNombre();
	}

	
}
